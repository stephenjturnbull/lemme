Here's a rough outline of the approach for the authenticating proxy server, as
sketched out at Pycon 2016.  This will require support in the core, and
additional fleshing out.  We don't expect to support this until Mailman 3.2.

* proxy authenticates user
* proxy asks core for all attributes for the user

  * pref address
  * second addresses
  * (including verified on)
  * memberships + roles
  * domains they own
  * server owner?
* proxy traverses core api in resource discovery mode

  * maybe special verb
  * maybe special header
* core returns unique id for resource
* proxy maps resource + user attributes -> permission
* proxy:

  * denies with http error code
  * allows by wildcard calling core in normal mode

We plan to use HTTP OPTION (hopefully/expected support from Falcon) as the way
the proxy queries the core for the required information.
